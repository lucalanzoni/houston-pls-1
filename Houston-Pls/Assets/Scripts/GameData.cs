﻿/*
    TODO:
        - inserire dati evento per Evento
        - (x Manuel) chiedere per il 300f e stop livello @t = 240
        - (x Manuel) chiedere come funziona {blood = 1} ect...
            

    per lo stop basta che cambiate 300 col valore che volete, è espresso in secondi

    per la modifica dei parametri:
    {
        radiation = +-n,

        muscle = +-n,

        blood = +-n,

        sight = +-n,

        psych = +-n,

        coordination = +-n,
    }

    dove n è uguale alla variazione che volete dare al parametro

    ho aggiunto il parametro logText agli eventi per indicare cosa volete scritto nel log. Di default ho messo logText = "Incoming call!"
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{

    // first level (easy)
        // first event
            public static Evento.Answer l1_firstEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_firstEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_firstEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_firstEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_firstEventAnswer1, l1_firstEventAnswer2, l1_firstEventAnswer3 } } ;
        // second event
            public static Evento.Answer l1_secondEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_secondEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_secondEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_secondEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_secondEventAnswer1, l1_secondEventAnswer2, l1_secondEventAnswer3 } } ;
        // third event
            public static Evento.Answer l1_thirdEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_thirdEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_thirdEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_thirdEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_thirdEventAnswer1, l1_thirdEventAnswer2, l1_thirdEventAnswer3 } } ;
        // fourth event
            public static Evento.Answer l1_fourthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_fourthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_fourthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_fourthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_fourthEventAnswer1, l1_fourthEventAnswer2, l1_fourthEventAnswer3 } } ;
        // fifth event
            public static Evento.Answer l1_fifthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_fifthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_fifthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_fifthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_fifthEventAnswer1, l1_fifthEventAnswer2, l1_fifthEventAnswer3 } } ;
        // sixth event
            public static Evento.Answer l1_sixthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_sixthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_sixthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_sixthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_sixthEventAnswer1, l1_sixthEventAnswer2, l1_sixthEventAnswer3 } } ;
        // seventh event
            public static Evento.Answer l1_seventhEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_seventhEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_seventhEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_seventhEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_seventhEventAnswer1, l1_seventhEventAnswer2, l1_seventhEventAnswer3 } } ;
        // eight event
            public static Evento.Answer l1_eightEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l1_eightEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l1_eightEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l1_eightEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l1_eightEventAnswer1, l1_eightEventAnswer2, l1_eightEventAnswer3 } } ;

    // second level (hard)
        // first event
            public static Evento.Answer l2_firstEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_firstEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_firstEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_firstEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_firstEventAnswer1, l2_firstEventAnswer2, l2_firstEventAnswer3 } } ;
        // second event
            public static Evento.Answer l2_secondEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_secondEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_secondEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_secondEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_secondEventAnswer1, l2_secondEventAnswer2, l2_secondEventAnswer3 } } ;
        // third event
            public static Evento.Answer l2_thirdEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_thirdEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_thirdEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_thirdEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_thirdEventAnswer1, l2_thirdEventAnswer2, l2_thirdEventAnswer3 } } ;
        // fourth event
            public static Evento.Answer l2_fourthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_fourthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_fourthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_fourthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_fourthEventAnswer1, l2_fourthEventAnswer2, l2_fourthEventAnswer3 } } ;
        // fifth event
            public static Evento.Answer l2_fifthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_fifthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_fifthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_fifthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_fifthEventAnswer1, l2_fifthEventAnswer2, l2_fifthEventAnswer3 } } ;
        // sixth event
            public static Evento.Answer l2_sixthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_sixthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_sixthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_sixthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_sixthEventAnswer1, l2_sixthEventAnswer2, l2_sixthEventAnswer3 } } ;
        // seventh event
            public static Evento.Answer l2_seventhEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_seventhEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_seventhEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_seventhEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_seventhEventAnswer1, l2_seventhEventAnswer2, l2_seventhEventAnswer3 } } ;
        // eight event
            public static Evento.Answer l2_eightEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_eightEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_eightEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_eightEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_eightEventAnswer1, l2_eightEventAnswer2, l2_eightEventAnswer3 } } ;
        // nineth event
            public static Evento.Answer l2_ninethEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_ninethEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_ninethEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_ninethEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_ninethEventAnswer1, l2_ninethEventAnswer2, l2_ninethEventAnswer3 } } ;
        // tenth event
            public static Evento.Answer l2_tenthEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
            public static Evento.Answer l2_tenthEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
            public static Evento.Answer l2_tenthEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
            public static Evento l2_tenthEvent = new Evento() { imageId = 0, content = new List<string>() { "test1", "test2" }, answers = new Evento.Answer[] { l2_tenthEventAnswer1, l2_tenthEventAnswer2, l2_tenthEventAnswer3 } } ;


    // levels 
        public static Level firstLevel = new Level() { events = new List<Evento>() { l1_firstEvent, l1_secondEvent, l1_thirdEvent, l1_fourthEvent, l1_fifthEvent, l1_sixthEvent, l1_seventhEvent, l1_eightEvent }, nextEvent = new List<float>() { 1, 1, 1, 1, 1, 1, 1, 2}, timer = 300f };
        public static Level secondLevel = new Level() { events = new List<Evento>() { l2_firstEvent, l2_secondEvent, l2_thirdEvent, l2_fourthEvent, l2_fifthEvent, l2_sixthEvent, l2_seventhEvent, l2_eightEvent, l2_ninethEvent, l2_tenthEvent}, nextEvent = new List<float>() { 5, 40, 75, 105, 130, 155, 180, 200, 215, 225 }, timer = 300f };
        
}
