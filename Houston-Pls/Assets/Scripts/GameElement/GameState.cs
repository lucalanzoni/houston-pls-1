﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState 
{

    public int radiation = 0;

    public int muscle = 0;

    public int blood = 0;

    public int sight = 0;

    public int psych = 0;

    public int coordination = 0;


    public void parameterVariation(GameState variationMask)
    {
        radiation += variationMask.radiation;
        muscle += variationMask.muscle;
        blood += variationMask.blood;
        sight += variationMask.sight;
        psych += variationMask.psych;
        coordination += variationMask.coordination;
    }
}
