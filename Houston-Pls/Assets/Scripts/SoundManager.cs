﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] soundArray;
    public AudioClip[] soundEffects;
    public AudioClip gameOverClip;
    public AudioClip victoryClip;
    private AudioSource audioSource;

    public Slider volumeSlider;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefs.GetFloat("volume", 1f);
        //audioSource.PlayOneShot(soundArray[0], 1.0f);
        //PlayOST(8);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Imposta il volume e lo memorizza nei player pref.
    /// </summary>
    /// <param name="volume">il volume</param>
    public void SetVolume()
    {
        float volume = volumeSlider.value;
        PlayerPrefs.SetFloat("volume", volume);
        audioSource.volume = volume;
    }


    private IEnumerator PlaySoundCoorutine(int soundID, float soundDuration)
    {
        audioSource.PlayOneShot(soundEffects[soundID], 1.0f);
        yield return new WaitForSeconds(soundDuration);
        //audioSource.Stop();
    }

    private IEnumerator PlaySoundCoorutine(AudioClip clip, float soundDuration)
    {
        audioSource.PlayOneShot(clip, 1.0f);
        yield return new WaitForSeconds(soundDuration);
    }

    public void PlaySound(AudioClip clip, float soundDuration = 1f)
    {
        StartCoroutine(PlaySoundCoorutine(clip, soundDuration));
    }

    public void PlayOST(int soundID)
    {
        audioSource.Stop();
        audioSource.clip = soundArray[soundID];
        audioSource.Play();
    }

    public void PlayOST(AudioClip myClip)
    {
        audioSource.Stop();
        audioSource.clip = myClip;
        audioSource.Play();
    }

    public void GameOverSound()
    {
        audioSource.Stop();
        StartCoroutine(PlaySoundCoorutine(gameOverClip, gameOverClip.length));
    }
    public void VictorySound()
    {
        audioSource.Stop();
        StartCoroutine(PlaySoundCoorutine(victoryClip, victoryClip.length));
    }


    public void ButtonNeutralEffect()
    {
        StartCoroutine(PlaySoundCoorutine(0, 1f));
    }

    public void ButtonBackEffect()
    {
        StartCoroutine(PlaySoundCoorutine(1, 1f));
    }

    public void ButtonStartEffect()
    {
        StartCoroutine(PlaySoundCoorutine(2, 1f));
    }

    public void PopUpSoundEffect()
    {
        StartCoroutine(PlaySoundCoorutine(3, 1f));
    }

    public void LevelUpSoundEffect()
    {
        StartCoroutine(PlaySoundCoorutine(4, 1f));
    }

}
